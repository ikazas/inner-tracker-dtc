-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Device specific transceiver
-------------------------------------------------------
-- Ozgur : added MGT GTY and GTH selection -  01.02.22

--! IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_framework_decl.all;

--! Include the LpGBT-FPGA specific package
library lpgbt_lib;
use lpgbt_lib.lpgbtfpga_package.all;


--! Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

--! @brief MGT - Transceiver
--! @details
--! The MGT module provides the interface to the transceivers to send the GBT-links via
--! high speed links (@4.8Gbps)
entity emp_lpgbt_mgt_10g24 is
 generic (
   N_CHANNELS: integer              := 4;
   GT_SEL:     io_gt_kind_t         := io_gty);
   port (
       --=============--
       -- Clocks      --
       --=============--
       MGT_REFCLK_i                 : in  std_logic;
       MGT_FREEDRPCLK_i             : in  std_logic;

       MGT_RXUSRCLK_o               : out std_logic;
       MGT_TXUSRCLK_o               : out std_logic;

       --=============--
       -- Resets      --
       --=============--
       MGT_TXRESET_i                : in  std_logic;
       MGT_RXRESET_i                : in  std_logic;

       --=============--
       -- Control     --
       --=============--
       MGT_TXPolarity_i             : in  std_logic;	                -- v2.1
       MGT_RXPolarity_i             : in  std_logic;	                -- v2.1

       MGT_RXSlide_i                : in  std_logic;

       MGT_ENTXCALIBIN_i            : in  std_logic;
       MGT_TXCALIB_i                : in  std_logic_vector(6 downto 0);

       --=============--
       -- Status      --
       --=============--
       MGT_TXREADY_o                : out std_logic;
       MGT_RXREADY_o                : out std_logic;

       MGT_TX_ALIGNED_o             : out std_logic;
       MGT_TX_PIPHASE_o             : out std_logic_vector(6 downto 0);
       --==============--
       -- Data         --
       --==============--
       MGT_USRWORD_i                : in  std_logic_vector(31 downto 0);
       MGT_USRWORD_o                : out std_logic_vector(31 downto 0);

       --===============--
       -- Serial intf.  --
       --===============--
       RXn_i                        : in  std_logic;
       RXp_i                        : in  std_logic;

       TXn_o                        : out std_logic;
       TXp_o                        : out std_logic
   );
end emp_lpgbt_mgt_10g24;

--! @brief MGT - Transceiver
--! @details The MGT module implements all the logic required to send the GBT frame on high speed
--! links: resets modules for the transceiver, Tx PLL and alignement logic to align the received word with the
--! GBT frame header.
architecture structural of emp_lpgbt_mgt_10g24 is
    --================================ Signal Declarations ================================--

   -- Reset signals
    signal tx_reset_done                    : std_logic;
    signal txfsm_reset_done                 : std_logic;
    signal rx_reset_done                    : std_logic;
    signal rxfsm_reset_done                 : std_logic;

    signal rxBuffBypassRst                  : std_logic;
    signal gtwiz_userclk_rx_active_int      : std_logic;
    signal gtwiz_buffbypass_rx_reset_in_s   : std_logic;
    signal gtwiz_userclk_tx_active_int      : std_logic;
    signal gtwiz_buffbypass_tx_reset_in_s   : std_logic;

    signal gtwiz_userclk_tx_reset_int       : std_logic;
    signal gtwiz_userclk_rx_reset_int       : std_logic;
    signal txpmaresetdone                   : std_logic;
    signal rxpmaresetdone                   : std_logic;

    signal rx_reset_sig                     : std_logic;
    signal tx_reset_sig                     : std_logic;

    signal MGT_USRWORD_s                    : std_logic_vector(31 downto 0);

    -- Clock signals
    signal rx_wordclk_sig                   : std_logic;
    signal tx_wordclk_sig                   : std_logic;
    signal rxoutclk_sig                     : std_logic;
    signal txoutclk_sig                     : std_logic;

    -- Tx phase aligner signals
    signal txbufstatus_s                    : std_logic_vector(1 downto 0);
    signal txpippmen_s                      : std_logic;
    signal txpippmovrden_s                  : std_logic;
    signal txpippmsel_s                     : std_logic;
    signal txpippmpd_s                      : std_logic;
    signal txpippmstepsize_in               : std_logic_vector(4 DOWNTO 0);
    signal drpaddr_s                        : std_logic_vector(9 downto 0);
    signal drpclk_s                         : std_logic;
    signal drpdi_s                          : std_logic_vector(15 downto 0);
    signal drpen_s                          : std_logic;
    signal drpwe_s                          : std_logic;
    signal drprdy_s                         : std_logic;
    signal drpdo_s                          : std_logic_vector(15 downto 0);

    signal tx_reset_hptd_ip                 : std_logic;
    signal tx_reset_hptd_ip_sync            : std_logic;
    signal MGT_TX_ALIGNED_s                 : std_logic;
	  signal MGT_TX_ALIGNED_sync              : std_logic;

    signal tx_phase_aligner_drpaddr_s       : std_logic_vector(8 downto 0);

		constant drp_addr_txpi_ppm_cfg          : std_logic_vector(8 downto 0) := "010011010";
    constant tx_fifo_fill_pd_max   	        : std_logic_vector := x"00400000";
		constant tx_fine_realign                : std_logic := '0';
		constant ps_strobe                      : std_logic := '0';
		constant ps_inc_ndec                    : std_logic := '0';
		constant ps_phase_step                  : std_logic_vector(3 downto 0) := (others => '0');

--=================================================================================================--
begin                 --========####   Architecture Body   ####========--
--=================================================================================================--
     --==================================== User Logic =====================================--

    --=============--
    -- Assignments --
    --=============--
    MGT_TXREADY_o          <= tx_reset_done and MGT_TX_ALIGNED_sync;

    txAlignedSynch_tx: entity work.emp_reset_synchronizer
            PORT MAP(
               clk_in                                   => tx_wordclk_sig,
               rst_in                                   => MGT_TX_ALIGNED_s,
               rst_out                                  => MGT_TX_ALIGNED_sync
            );
    MGT_TX_ALIGNED_o <= MGT_TX_ALIGNED_sync;

    tx_reset_hptd_ip       <= not(tx_reset_done);
    txPhaseAlignerResetSynch_sys: entity work.emp_reset_synchronizer
        PORT MAP(
           clk_in                                   => MGT_FREEDRPCLK_i,
           rst_in                                   => tx_reset_hptd_ip,
           rst_out                                  => tx_reset_hptd_ip_sync
        );

    MGT_RXREADY_o          <= rx_reset_done and rxfsm_reset_done;

    MGT_RXUSRCLK_o         <= rx_wordclk_sig;
    MGT_TXUSRCLK_o         <= tx_wordclk_sig;

    rx_reset_sig           <= MGT_RXRESET_i or not(tx_reset_done and MGT_TX_ALIGNED_s); -- removed the reset on not tx ready
    tx_reset_sig           <= MGT_TXRESET_i;

    rxBuffBypassRst        <= not(gtwiz_userclk_rx_active_int) or (not(tx_reset_done) and not(MGT_TX_ALIGNED_s)); -- removed the reset on not tx ready.

   resetDoneSynch_rx: entity work.emp_reset_synchronizer
    PORT MAP(
       clk_in                                   => rx_wordClk_sig,
       rst_in                                   => rxBuffBypassRst,
       rst_out                                  => gtwiz_buffbypass_rx_reset_in_s
    );

    gtwiz_userclk_tx_reset_int <= not(txpmaresetdone);
    gtwiz_userclk_rx_reset_int <= not(rxpmaresetdone);

    rxWordClkBuf_inst: bufg_gt
    port map (
     O                                        => rx_wordclk_sig,
     I                                        => rxoutclk_sig,
--     CE                                       => not(gtwiz_userclk_rx_reset_int),
     CE                                       => '1',
     DIV                                      => "000",
     CLR                                      => '0',
     CLRMASK                                  => '0',
     CEMASK                                   => '0'
    );

    txWordClkBuf_inst: bufg_gt
    port map (
     O                                        => tx_wordclk_sig,
     I                                        => txoutclk_sig,
--     CE                                       => not(gtwiz_userclk_tx_reset_int),
     CE                                       => '1',
     DIV                                      => "000",
     CLR                                      => '0',
     CLRMASK                                  => '0',
     CEMASK                                   => '0'
    );


    activetxUsrClk_proc: process(gtwiz_userclk_tx_reset_int, tx_wordclk_sig)
    begin
    if gtwiz_userclk_tx_reset_int = '1' then
        gtwiz_userclk_tx_active_int <= '0';
    elsif rising_edge(tx_wordclk_sig) then
        gtwiz_userclk_tx_active_int <= '1';
    end if;

    end process;


    activerxUsrClk_proc: process(gtwiz_userclk_rx_reset_int, rx_wordclk_sig)
    begin
    if gtwiz_userclk_rx_reset_int = '1' then
        gtwiz_userclk_rx_active_int <= '0';
    elsif rising_edge(rx_wordclk_sig) then
        gtwiz_userclk_rx_active_int <= '1';
    end if;

    end process;


    rxWordPipeline_proc: process(rx_reset_done, rx_wordclk_sig)
    begin
      if rx_reset_done = '0' then
          MGT_USRWORD_o <= (others => '0');
      elsif rising_edge(rx_wordclk_sig) then
          MGT_USRWORD_o <= MGT_USRWORD_s;
      end if;
    end process;





    mgt_gth_gen: if GT_SEL = io_gth generate
    xlx_gth_std_i: entity work.lpgbt_mgt_ip_gth_10g24
    PORT MAP (
        rxusrclk_in(0) => rx_wordclk_sig,
        rxusrclk2_in(0) => rx_wordclk_sig,
        rxoutclk_out(0) => rxoutclk_sig,
        txusrclk_in(0) => tx_wordclk_sig,
        txusrclk2_in(0) => tx_wordclk_sig,
        txoutclk_out(0) => txoutclk_sig,

        gtwiz_userclk_tx_reset_in(0)  => std_logic'('0'),
        gtwiz_userclk_tx_active_in(0) => gtwiz_userclk_tx_active_int,
        gtwiz_userclk_rx_active_in(0) => gtwiz_userclk_rx_active_int,

        gtwiz_buffbypass_rx_reset_in(0) => gtwiz_buffbypass_rx_reset_in_s,
        gtwiz_buffbypass_rx_start_user_in(0) => std_logic'('0'),
        gtwiz_buffbypass_rx_done_out(0) => rxfsm_reset_done,
        gtwiz_buffbypass_rx_error_out => open,

        gtwiz_reset_clk_freerun_in(0) => MGT_FREEDRPCLK_i,

        gtwiz_reset_all_in(0) => std_logic'('0'),
        gtwiz_reset_tx_pll_and_datapath_in(0) => tx_reset_sig,
        gtwiz_reset_tx_datapath_in(0) => std_logic'('0'),
        gtwiz_reset_tx_done_out(0) => tx_reset_done,

        gtwiz_reset_rx_pll_and_datapath_in(0) => std_logic'('0'),
        gtwiz_reset_rx_datapath_in(0) => rx_reset_sig,
        gtwiz_reset_rx_cdr_stable_out => open,
        gtwiz_reset_rx_done_out(0) => rx_reset_done,

        gtwiz_userdata_tx_in => MGT_USRWORD_i,
        gtwiz_userdata_rx_out => MGT_USRWORD_s,

    -- DRP bus
        drpclk_in(0) => MGT_FREEDRPCLK_i,

        gthrxn_in(0) => RXn_i,
        gthrxp_in(0) => RXp_i,
        gthtxn_out(0) => TXn_o,
        gthtxp_out(0) => TXp_o,

        gtrefclk0_in(0) => MGT_REFCLK_i,

        rxslide_in(0) => MGT_RXSlide_i,

        txpolarity_in(0)                      => MGT_TXPolarity_i,
        rxpolarity_in(0)                      => MGT_RXPolarity_i,

        rxpmaresetdone_out(0) => rxpmaresetdone,
        txpmaresetdone_out(0) => txpmaresetdone,

        drpaddr_in => drpaddr_s,
        drpdi_in => drpdi_s,
        drpen_in(0) => drpen_s,
        drpwe_in(0) => drpwe_s,
        drpdo_out => drpdo_s,
        drprdy_out(0) => drprdy_s,


        -- PI control / monitoring signals
        txpippmen_in(0) => txpippmen_s,
        txpippmovrden_in(0) => txpippmovrden_s,
        txpippmpd_in(0) => txpippmpd_s,
        txpippmsel_in(0) => txpippmsel_s,
        txpippmstepsize_in => txpippmstepsize_in,

        txbufstatus_out => txbufstatus_s,

        rxcommadeten_in => std_logic_vector'("1"),
        rxmcommaalignen_in => std_logic_vector'("0"),
        rxpcommaalignen_in => std_logic_vector'("0")
         );
    end generate;
    mgt_gty_gen: if GT_SEL = io_gty generate
    xlx_gty_std_i: entity work.lpgbt_mgt_ip_gty_10g24
    PORT MAP (
        rxusrclk_in(0) => rx_wordclk_sig,
        rxusrclk2_in(0) => rx_wordclk_sig,
        rxoutclk_out(0) => rxoutclk_sig,
        txusrclk_in(0) => tx_wordclk_sig,
        txusrclk2_in(0) => tx_wordclk_sig,
        txoutclk_out(0) => txoutclk_sig,

        --gtwiz_userclk_tx_reset_in(0)  => '0',
        gtwiz_userclk_tx_active_in(0) => gtwiz_userclk_tx_active_int,
        gtwiz_userclk_rx_active_in(0) => gtwiz_userclk_rx_active_int,

        gtwiz_buffbypass_rx_reset_in(0) => gtwiz_buffbypass_rx_reset_in_s,
        gtwiz_buffbypass_rx_start_user_in(0) => '0',
        gtwiz_buffbypass_rx_done_out(0) => rxfsm_reset_done,
        gtwiz_buffbypass_rx_error_out => open,

        gtwiz_reset_clk_freerun_in(0) => MGT_FREEDRPCLK_i,

        gtwiz_reset_all_in(0) => '0',
        gtwiz_reset_tx_pll_and_datapath_in(0) => tx_reset_sig,
        gtwiz_reset_tx_datapath_in(0) => '0',
        gtwiz_reset_tx_done_out(0) => tx_reset_done,

        gtwiz_reset_rx_pll_and_datapath_in(0) => '0',
        gtwiz_reset_rx_datapath_in(0) => rx_reset_sig,
        gtwiz_reset_rx_cdr_stable_out => open,
        gtwiz_reset_rx_done_out(0) => rx_reset_done,

        gtwiz_userdata_tx_in => MGT_USRWORD_i,
        gtwiz_userdata_rx_out => MGT_USRWORD_s,

    -- DRP bus
        drpclk_in(0) => MGT_FREEDRPCLK_i,

        gtyrxn_in(0) => RXn_i,
        gtyrxp_in(0) => RXp_i,
        gtytxn_out(0) => TXn_o,
        gtytxp_out(0) => TXp_o,

        gtrefclk00_in(0) => MGT_REFCLK_i,

        rxslide_in(0) => MGT_RXSlide_i,

        txpolarity_in(0)                      => MGT_TXPolarity_i,
        rxpolarity_in(0)                      => MGT_RXPolarity_i,

        rxpmaresetdone_out(0) => rxpmaresetdone,
        txpmaresetdone_out(0) => txpmaresetdone,

        drpaddr_in => drpaddr_s,
        drpdi_in => drpdi_s,
        drpen_in(0) => drpen_s,
        drpwe_in(0) => drpwe_s,
        drpdo_out => drpdo_s,
        drprdy_out(0) => drprdy_s,


        -- PI control / monitoring signals
        txpippmen_in(0) => txpippmen_s,
        txpippmovrden_in(0) => txpippmovrden_s,
        txpippmpd_in(0) => txpippmpd_s,
        txpippmsel_in(0) => txpippmsel_s,
        txpippmstepsize_in => txpippmstepsize_in,

        txbufstatus_out => txbufstatus_s,

        rxcommadeten_in => "1",
        rxmcommaalignen_in => "0",
        rxpcommaalignen_in => "0"
         );
    end generate;
   drpaddr_s <= '0' & tx_phase_aligner_drpaddr_s;
   cmp_tx_phase_aligner : entity work.tx_phase_aligner
      generic map(
        g_DRP_NPORT_CTRL        => true,
        g_DRP_ADDR_TXPI_PPM_CFG => drp_addr_txpi_ppm_cfg
        )
      port map(
        --==============================================================================
        --! User control/monitor ports
        --==============================================================================
        -- Clock / reset
        clk_sys_i             => MGT_FREEDRPCLK_i     ,
        reset_i               => tx_reset_hptd_ip_sync,

        tx_aligned_o          => MGT_TX_ALIGNED_s ,

        tx_pi_phase_calib_i   => MGT_TXCALIB_i    ,
        tx_ui_align_calib_i   => MGT_ENTXCALIBIN_i,
        tx_fifo_fill_pd_max_i => tx_fifo_fill_pd_max     ,

        tx_fine_realign_i     => tx_fine_realign,

        ps_strobe_i           => ps_strobe,
        ps_inc_ndec_i         => ps_inc_ndec,
        ps_phase_step_i       => ps_phase_step,
        ps_done_o             => open,

        tx_pi_phase_o         => MGT_TX_PIPHASE_o,

        tx_fifo_fill_pd_o     => open,

        --==============================================================================
        --! MGT ports
        --==============================================================================
        clk_txusr_i           => tx_wordclk_sig    ,

        tx_fifo_fill_level_i  => txbufstatus_s(0)  ,

        txpippmen_o           => txpippmen_s       ,
        txpippmovrden_o       => txpippmovrden_s   ,
        txpippmsel_o          => txpippmsel_s      ,
        txpippmpd_o           => txpippmpd_s       ,
        txpippmstepsize_o     => txpippmstepsize_in,

        drpaddr_o             => tx_phase_aligner_drpaddr_s,
        drpen_o               => drpen_s,
        drpdi_o               => drpdi_s,
        drprdy_i              => drprdy_s,
        drpdo_i               => drpdo_s,
        drpwe_o               => drpwe_s
        );


end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
