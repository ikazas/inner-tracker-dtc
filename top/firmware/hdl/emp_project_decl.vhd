
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment when adding LPGBT links
library lpgbt_lib;
use lpgbt_lib.lpgbtfpga_package.all;

use work.emp_framework_decl.all;
use work.emp_device_decl.all;
use work.emp_device_types.all;
use work.emp_lpgbt_decl.all;
use work.emp_data_framer_decl.all;
use work.emp_slink_types.all;


package emp_project_decl is

  constant PAYLOAD_REV : std_logic_vector(31 downto 0) := X"cafebeef";

  -- Latency buffer size
  constant LB_ADDR_WIDTH : integer             := 10;

  -- Clock setup
  constant CLOCK_COMMON_RATIO : integer               := 36;
  constant CLOCK_RATIO        : integer               := 9;
  constant CLOCK_AUX_DIV      : clock_divisor_array_t := (18, 9, 4); -- Dividers of CLOCK_COMMON_RATIO * 40 MHz
   
  -- Only used by nullalgo   
  constant PAYLOAD_LATENCY : integer := 5;

  constant REGION_CONF : region_conf_array_t := (
       0   => kDummyRegion,                     -- service c2c
       1   => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
      --  2   => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
       3   => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
       4   => kDummyRegion,                     -- not routed
       5   => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
      --  6   => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
       7   => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
      --  8   => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
       9   => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
      --  10  => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
       11  => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
      --  12  => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
       13  => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
       14  => (gty25, buf, no_fmt, buf, gty25), -- fpga-fpga
       15  => kDummyRegion,                     -- not routed
       ------cross
       16  => kDummyRegion,                      -- not routed
       17  => (lpgbt, buf, no_fmt, buf, lpgbt),  -- firefly
      --  18  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
       19  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
       20  => kDummyRegion,                      -- not routed                  
       21  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
      --  22  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
       23  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
      --  24  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
       25  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
      --  26  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
       27  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
      --  28  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
       29  => (gty25, buf, no_fmt, buf, gty25),  -- firefly
       30  => (gty25, buf, no_fmt, buf, gty25),  -- firefly     
       31 => kDummyRegion,                       -- service tcds

       others => kDummyRegion
    );

  -- for data framer (ic_simple, no_ec, n_ec_spare, ec_broadcast)
  constant REGION_DATA_FRAMER_CONF : region_data_framer_conf_array_t := (
    17 => ( 0=>(false, true, 0, false), 1=>(false, true, 0, false), 2=>(false, true, 0, false), 3=>(false, true, 0, false)),
    others => kDummyRegionDataFramer
  );

  -- for lpgbt
  constant REGION_LPGBT_CONF : region_lpgbt_conf_array_t := (
    17  => (FEC5, DATARATE_5G12, PCS),
    others => kDummyRegionLpgbt
  );


  -- Specify the slink quad using the corresponding region conf ID
  -- Specify slink channels to enable using the channel mask
  constant SLINK_CONF : slink_conf_array_t := (
    --0 => (27, x"3"),
    others      => kNoSlink
    );  
  
  
end emp_project_decl;
