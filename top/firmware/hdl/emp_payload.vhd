-- null_algo
--
-- Do-nothing top level algo for testing
--
-- Dave Newbold, July 2013
-- Adding slink outputs and ipbus config : Kristian Hahn, 2022

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_MISC.ALL; -- or_reduce

use work.ipbus.all;
use work.emp_data_types.all;
use work.emp_project_decl.all;

use work.emp_device_decl.all;
use work.emp_ttc_decl.all;

use work.emp_slink_types.all;

entity emp_payload is
  port(
    clk         : in  std_logic;        -- ipbus signals
    rst         : in  std_logic;
    ipb_in      : in  ipb_wbus;
    ipb_out     : out ipb_rbus;
    clk40       : in  std_logic;	
    clk_payload : in  std_logic_vector(2 downto 0);
    rst_payload : in  std_logic_vector(2 downto 0);
    clk_p       : in  std_logic;        -- data clock
    rst_loc     : in  std_logic_vector(N_REGION - 1 downto 0);
    clken_loc   : in  std_logic_vector(N_REGION - 1 downto 0);
    ctrs        : in  ttc_stuff_array;
    bc0         : out std_logic;
    d           : in  ldata(4 * N_REGION - 1 downto 0);  -- data in
    q           : out ldata(4 * N_REGION - 1 downto 0);  -- data out
    gpio        : out std_logic_vector(29 downto 0);  -- IO to mezzanine connector
    gpio_en     : out std_logic_vector(29 downto 0);  -- IO to mezzanine connector (three-state enables)
    slink_q : out slink_input_data_quad_array(SLINK_MAX_QUADS-1 downto 0);
    backpressure : in std_logic_vector(SLINK_MAX_QUADS-1 downto 0)
    );

end emp_payload;

architecture rtl of emp_payload is

  type dr_t is array(PAYLOAD_LATENCY downto 0) of ldata(3 downto 0);

  signal ipb_w : ipb_wbus;
  signal ipb_r : ipb_rbus;

  signal slink_i : slink_input_data_array;
  signal backpressure_any : std_logic;

  component emp_slink_generator is
    Generic (
      channel_mask : std_logic_vector(SLINK_MAX_CHANNELS-1 downto 0)
      );
    Port (
      ipb_clk      : in std_logic;
      ipb_rst      : in std_logic;
      ipb_in       : in ipb_wbus;
      ipb_out      : out ipb_rbus;

      rst_p        : in std_logic;
      clk_p        : in std_logic;

      data_q       : out slink_input_data_array;
      backpressure : in std_logic
      );
  end component;

begin

  gen : for i in N_REGION - 1 downto 0 generate

    constant ich : integer := i * 4 + 3;
    constant icl : integer := i * 4;
    signal dr    : dr_t;

    attribute SHREG_EXTRACT       : string;
    attribute SHREG_EXTRACT of dr : signal is "no";  -- Don't absorb FFs into shreg

  begin

    dr(0) <= d(ich downto icl);

    process(clk_p)                      -- Mother of all shift registers
    begin
      if rising_edge(clk_p) then
        dr(PAYLOAD_LATENCY downto 1) <= dr(PAYLOAD_LATENCY - 1 downto 0);
      end if;
    end process;

    q(ich downto icl) <= dr(PAYLOAD_LATENCY);

  end generate gen;

  bc0 <= '0';

  gpio    <= (others => '0');
  gpio_en <= (others => '0');


  --
  -- SLINK
  --
  dummy : entity work.emp_slink_generator
    generic map ( channel_mask => x"F" )
    port map (
      ipb_clk      => clk,
      ipb_rst      => rst,
      ipb_in       => ipb_in,
      ipb_out      => ipb_out,

      rst_p        => '0',
      clk_p        => clk_p,

      data_q       => slink_i,
      backpressure => backpressure_any
      );

  slink_q_gen : for q in SLINK_MAX_QUADS-1 downto 0 generate
  begin
    slink_q(q) <= slink_i;
  end generate;
  backpressure_any <= or_reduce(backpressure(SLINK_MAX_QUADS-1 downto 0));

end rtl;
