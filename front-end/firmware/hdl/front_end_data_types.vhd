library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gbt_module_constants.all;

package front_end_data_types is

type tAlignerArray is array(natural range <>) of  std_logic_vector(3 downto 0);


end package front_end_data_types;


package body front_end_data_types is

end front_end_data_types;
